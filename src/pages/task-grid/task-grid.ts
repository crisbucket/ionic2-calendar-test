import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import * as moment from 'moment';

/**
 * Generated class for the TaskGridPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-task-grid',
  templateUrl: 'task-grid.html',
})
export class TaskGridPage {

  week: moment.Moment[] = [];

  weekDays: string[] = [];
  employees: Employee[] = [
    {
      name: 'Jan',
      tasks: [
        {
          name: 'ING',
          description: 'reklama dla ING',
          start: moment().add(1, 'days').startOf('days'),
          end: moment().add(1, 'days').endOf('days'),
          finished: true
        },
        {
          name: 'Millenium',
          description: 'reklama dla Millenium',
          start: moment().subtract(1, 'days').startOf('days'),
          end: moment().add(2, 'days').endOf('days'),
          finished: false
        }
      ]
    },
    {
      name: 'Kazik',
      tasks: [
        {
          name: 'Kaufland',
          description: 'reklama dla Kaufland',
          start: moment().add(2, 'days').startOf('days'),
          end: moment().add(3, 'days').endOf('days'),
          finished: false
        },
        {
          name: 'Lidl',
          description: 'reklama dla Lidl',
          start: moment().subtract(1, 'days').startOf('days'),
          end: moment().add(4, 'days').endOf('days'),
          finished: true
        }
      ]
    }
  ];

  jakis: any = moment().subtract(1, 'days').toISOString();
  nazwyMiesiecy: string[] =[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    moment.locale('pl');
    for (var i = 0; i < 7; i++) {
      this.week.push(moment().startOf('week').add(i, 'days'));
    }

    for (let day of this.week) {
      this.weekDays.push(day.format('dd'));
    }

  this.nazwyMiesiecy = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskGridPage');
  }

  public taskForTheDay(emp: Employee, day: moment.Moment): Task[] {
    return emp.tasks.filter((task: Task, index, array) => day.isBetween(task.start, task.end, 'days', '[]'))
  }

  public formatAsDay(m: moment.Moment): string {
    console.warn('heeeeeeeeeeeeeeeeee');
    console.warn(m);
    let ret = m.format('dd');
    console.warn(ret);
    return ret;
  }

}


interface Employee {
  name: string;
  tasks: Task[];
}


interface Task {
  name: string;
  description: string;
  start: moment.Moment;
  end: moment.Moment;
  finished: boolean;
}


