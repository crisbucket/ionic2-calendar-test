import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaskGridPage } from './task-grid';

@NgModule({
  declarations: [
    TaskGridPage,
  ],
  imports: [
    IonicPageModule.forChild(TaskGridPage),
  ],
})
export class TaskGridPageModule {}
