import {Component} from '@angular/core';
import {NavController, ModalController, AlertController} from 'ionic-angular';
import * as moment from 'moment';
import {DatePipe} from "@angular/common";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  eventSource = [];
  viewTitle: string;
  selectedDay = new Date();

  editors : Array<{key: number, name: string }> = [
    {key: 0, name: "Janek"},
    {key: 1, name: "Franek"},
    {key: 2, name: "Maniek"},
  ]

  calendar = {
    mode: 'month',
    currentDate: new Date()
  };

  constructor(public navCtrl: NavController, private modalCtrl: ModalController, private alertCtrl: AlertController) {
  }

  addEvent() {
    let modal = this.modalCtrl.create('EventModalPage', {selectedDay: this.selectedDay, editors: this.editors});
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        let eventData = data;

        eventData.startTime = new Date(data.startTime);
        eventData.endTime = new Date(data.endTime);

        let events = this.eventSource;
        events.push(eventData);
        this.eventSource = [];
        setTimeout(() => {
          this.eventSource = events;
        });
      }
    });
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onEventSelected(event) {
    let datePipe = new DatePipe('pl');
    let start = datePipe.transform(event.startTime, 'LLLL');
    let end = datePipe.transform(event.endTime, 'LLLL');

    let alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: 'Od: ' + start + '<br>Do: ' + end + '<br>Szczegóły:<br>' + event.description + '<br>Edytor: ' + this.editors.find((value, index, obj) => value.key === event.editor).name,
      buttons: ['OK']
    })
    alert.present();
  }

  onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
  }
}
