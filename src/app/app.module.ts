import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';

import {NgCalendarModule} from "ionic2-calendar";

import {registerLocaleData} from "@angular/common";
import localePl from '@angular/common/locales/pl';
import {TaskGridPage} from "../pages/task-grid/task-grid";
import {DatePickerModule} from "datepicker-ionic2";

registerLocaleData(localePl);


// export const firebaseConfig: FirebaseAppConfig = {
//   apiKey: "AIzaSyAyAxQjq8bTaE-VvmN0k0bY0s3N-l23j88",
//   authDomain: "magda-kalendarz.firebaseapp.com",
//   databaseURL: "https://magda-kalendarz.firebaseio.com",
//   projectId: "magda-kalendarz",
//   storageBucket: "",
//   messagingSenderId: "1080056934218"
// };


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TaskGridPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    NgCalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TaskGridPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {
}
